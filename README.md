# crawler-engine

To run using fish:

```
env HOST=http://my.crawler.server node engine.js
```

* HOST: Server where the task and everything lives
* TOKEN: token to get access to the API server
